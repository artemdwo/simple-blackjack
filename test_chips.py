import unittest
from chips import Chips


class TestChipsClass(unittest.TestCase):

    def setUp(self):
        self.player_chips = Chips()

    def test_chips_init(self):
        self.assertEqual(100, self.player_chips.total)
        self.assertEqual(0, self.player_chips.bet)

    def test_chips_win_bet(self):
        self.player_chips.bet = 15
        self.player_chips.win_bet()
        self.assertEqual(115, self.player_chips.total)

    def test_chips_lose_bet(self):
        self.player_chips.bet = 15
        self.player_chips.lose_bet()
        self.assertEqual(85, self.player_chips.total)
