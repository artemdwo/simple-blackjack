import unittest
from deck import Deck
from hand import Hand


class TestHandClass(unittest.TestCase):

    def test_hand_init(self):
        players_hand = Hand()
        self.assertEqual(0, players_hand.value)
        self.assertEqual([], players_hand.cards)
        self.assertEqual(0, players_hand.aces)

    def test_deal_to_hand(self):
        a_deck = Deck()
        players_hand = Hand()
        players_hand.add_card(a_deck.deal())
        self.assertEqual(11, players_hand.value)
        self.assertEqual(1, players_hand.aces)

    def test_adjust_for_aces(self):
        a_deck = Deck()
        players_hand = Hand()
        for x in range(3):
            players_hand.add_card(a_deck.deal())
        # Check before adjustment
        self.assertEqual(31, players_hand.value)
        self.assertEqual(1, players_hand.aces)
        players_hand.adjust_for_ace()
        # Check after adjustment
        self.assertEqual(21, players_hand.value)
        self.assertEqual(0, players_hand.aces)
